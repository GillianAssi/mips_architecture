----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/22/2019 12:08:35 PM
-- Design Name: 
-- Module Name: TB_InstructionMemory - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Simulated_Instruction_Fetch IS
END Simulated_Instruction_Fetch;

-- Component Declaration for the Instruction Fetch (IF)

architecture  behavior OF Simulated_Instruction_Fetch IS

    COMPONENT TopLayer_Instruction_Fetch is
    Port ( clk : in STD_LOGIC;
    	   reset: in STD_LOGIC;
           PC_old : in STD_LOGIC_VECTOR (31 downto 0);
           MUXInput: in STD_LOGIC;
           ShiftLeftInput: in STD_LOGIC_VECTOR (31 downto 0);
           PC_new : out STD_LOGIC_VECTOR (31 downto 0);
           Instruction : out STD_LOGIC_VECTOR (31 downto 0));   
	end COMPONENT;
    
    --Inputs
    signal clk : std_logic := '0';
    signal reset : std_logic := '0';
    signal PC_old: std_logic_vector(31 downto 0) := (others => '0' );
    signal MUXInput: std_logic := '0';
    signal ShiftLeftInput: std_logic_vector(31 downto 0):= "00000000000000000000000000000010";
        
    --Outputs
    signal PC_new : std_logic_vector(31 downto 0) := (others => '0' );
    signal Instruction : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');   
    
    -- Clock period definitions
    constant clk_period : time := 10 ns;
    
BEGIN

    -- Instantiate the Instruction Fetch (IF)
    uut: TopLayer_Instruction_Fetch PORT MAP (
        clk => clk,
        reset => reset,
        PC_old => PC_old,
        MUXInput => MUXInput,
        ShiftLeftInput => ShiftLeftInput,
        PC_new => PC_new,
        Instruction => Instruction
    );
    
    -- Clock process definitions
    clk_process :process
    begin
        clk <= '1' ;
        wait for clk_period/2;
        clk <= '0' ;
        wait for clk_period/2;
    end process;
    
    -- Stimulus process
    stim_proc: process
    begin
        -- hold reset state for 100 ns.
        wait for 10 ns;
		--PC_old verhoogt met 4 na 10ns
        PC_old <= PC_new;
        wait for 10 ns;
        --PC_old verhoogt met 4 na 10ns
        PC_old <= PC_new;
        wait for 10 ns;
        --PC_old verhoogt met 4 na 10ns
        PC_old <= PC_new;
        wait for 10 ns;
        --PC_old verhoogt met 4 na 10ns
        MUXInput <= '1';
        PC_old<= PC_new;
        wait for 10ns;
        MUXInput <= '0';
        PC_old <= PC_new;
        wait;
    end process;
END;
