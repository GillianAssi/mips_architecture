----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/11/2020 12:41:28 PM
-- Design Name: 
-- Module Name: Simulation_UART_TX - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY Simulation_UART_RX IS
END Simulation_UART_RX;
 
ARCHITECTURE behavior OF Simulation_UART_RX IS
 
	-- Component Declaration for the Unit Under Test (UUT)
 
	COMPONENT UART_RX
	PORT(
                clk : IN  std_logic;
                --state : OUT STD_LOGIC_VECTOR(1 downto 0);
                RX : OUT  std_logic;
                RX_in : IN  std_logic;
                RX_data : OUT  std_logic_vector(7 downto 0)
        );
	END COMPONENT;
 
	--Inputs
	signal clk : std_logic := '0';
	signal RX_in : std_logic := '1';
	--Outputs
	signal RX : std_logic;
	signal RX_data : std_logic_vector(7 downto 0) := (others => '0');
	-- Clock period definitions
	constant clk_period : time := 10 ns;
 
	BEGIN
	-- Instantiate the Unit Under Test (UUT)
	uut: UART_RX PORT MAP (
                        clk => clk,
                        RX => RX,
                        RX_in => RX_in,
                        RX_data => RX_data
                        );
	-- Clock process definitions
	clk_process :process
	begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;
 
 
	-- Stimulus process
	stim_proc: process
	begin
		-- hold reset state for at least 100 ns.
		RX_in<='1';               --  signaal van de buitenaf ( van het aparraat dat zendt )
		wait for 130ns;
		RX_in<='0';
		-- start
		wait for 40ns;
        RX_in<='1';
        wait for 40ns;
        RX_in<='0';
        wait for 40ns;
        RX_in<='1';
		wait for 40ns;
        RX_in<='1';
        wait for 40ns;
        RX_in<='0';
        wait for 40ns;
        RX_in<='0';
        wait for 40ns;
        RX_in<='1';
        wait for 40ns;
        RX_in<='0';
		wait;
	end process;
END;