----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/26/2019 11:59:54 AM
-- Design Name: 
-- Module Name: Simulated_MIPS - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Simulated_MIPS is
end Simulated_MIPS;

architecture Behavioral of Simulated_MIPS is

COMPONENT Toplayer_MIPS is
    Port ( clk : in STD_LOGIC;
           PC_IN : in STD_LOGIC_VECTOR (31 downto 0);
           Reset : in STD_LOGIC;
           RX_in : in STD_LOGIC;
           UART_REC : out STD_LOGIC_VECTOR(31 downto 0);   -- data dat de UART receiver heeft ontvangen
           Instruction : out STD_LOGIC_VECTOR (31 downto 0);                        -- Instructie die uitgevoerd wordt
           ALUOp: out STD_LOGIC_VECTOR(1 downto 0);
           ALU_CTRL: out STD_LOGIC_VECTOR(3 downto 0);
           DATA1: out STD_LOGIC_VECTOR(31 downto 0);
           DATA2: out STD_LOGIC_VECTOR(31 downto 0);
           WRITEREG: out STD_LOGIC_VECTOR(4 downto 0);
           ALU_RESULT: out STD_LOGIC_VECTOR(31 downto 0);
           MULT_Result: out std_logic_vector(63 downto 0);
           MUXOutput : out STD_LOGIC;
           UARTWriteSignal: out std_logic;
           PC_OUT : out STD_LOGIC_VECTOR (31 downto 0);                             -- nieuwe PC die in PC_IN gestoken moet worden
           WrittenData: out STD_LOGIC_VECTOR(31 downto 0)                           -- data die terug gaat naar Register file 
           );
end COMPONENT;

--inputs
signal clk : std_logic := '0';
signal reset : std_logic := '0';
signal PC_IN : std_logic_vector(31 downto 0) := (others => '0');
signal RX_in : STD_LOGIC := '1';

--outputs
signal Instruction : std_logic_vector(31 downto 0);
signal PC_OUT :  std_logic_vector(31 downto 0) := (others => '0');
signal WrittenData : std_logic_vector(31 downto 0);
signal ALUOp: STD_LOGIC_VECTOR(1 downto 0);
signal ALU_CTRL: STD_LOGIC_VECTOR(3 downto 0);
signal ALU_RESULT: STD_LOGIC_VECTOR(31 downto 0);
signal DATA1: STD_LOGIC_VECTOR(31 downto 0);
signal DATA2: STD_LOGIC_VECTOR(31 downto 0);
signal WRITEREG: STD_LOGIC_VECTOR(4 downto 0);
signal MULT_Result: std_logic_vector(63 downto 0);
signal MUXOutput : STD_LOGIC;
signal UARTWriteSignal: std_logic := '0';
signal UART_REC: std_logic_vector(31 downto 0) := (others => '0');

constant clk_period : time := 10 ns;

begin

    MIPS: Toplayer_MIPS
    port map(   
            clk => clk,
            PC_IN => PC_IN,
            Reset => Reset,
            RX_in => RX_in,
            Instruction => Instruction,
            ALUOp => ALUOp,
            ALU_CTRL => ALU_CTRL,
            DATA1 => DATA1,
            DATA2 => DATA2,
            WRITEREG => WRITEREG,
            ALU_RESULT => ALU_RESULT,
            PC_OUT => PC_OUT,
            WrittenData => WrittenData,
            MUXOutput => MUXOutput,
            UARTWriteSignal => UARTWriteSignal,
            MULT_Result => MULT_Result,
            UART_REC => UART_REC
            );
            
    -- Clock process definitions
        clk_process :process
        begin
            clk <= '1' ;
            wait for clk_period/2;
            clk <= '0' ;
            wait for clk_period/2;
        end process;
        
    --UART receiver process
    	UART_proc: process
    begin
        -- hold reset state for at least 100 ns.
        RX_in<='1';               --  signaal van de buitenaf ( van het aparraat dat zendt )
        wait for 130ns;
        
        RX_in<='0';
        -- start
        wait for 40ns;
        RX_in<='1';
        wait for 40ns;
        RX_in<='0';
        wait for 40ns;
        RX_in<='1';
        wait for 40ns;
        RX_in<='0';
        wait for 40ns;
        RX_in<='0';
        wait for 40ns;
        RX_in<='1';
        wait for 40ns;
        RX_in<='1';
        wait for 40ns;
        RX_in<='0';
        wait;
    end process;
            
            
    -- Stimulus process
        stim_proc: process
        begin
            --wait for clk_period;
            --PC_IN <= PC_OUT;
            wait for 900ns;
            PC_IN <= PC_OUT;
            wait;
        end process;              


end Behavioral;
