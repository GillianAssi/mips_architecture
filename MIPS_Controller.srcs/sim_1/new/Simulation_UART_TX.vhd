----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/11/2020 12:41:28 PM
-- Design Name: 
-- Module Name: Simulation_UART_TX - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY Simulation_UART_TX IS
END Simulation_UART_TX;
 
ARCHITECTURE behavior OF Simulation_UART_TX IS
 
	-- Component Declaration for the Unit Under Test (UUT)
 
	COMPONENT UART_TX
	PORT(
		clk : IN  std_logic;
		--state : OUT STD_LOGIC_VECTOR(1 downto 0);
		TX : OUT  std_logic;
		TX_enable : IN  std_logic;
		TX_data : IN  std_logic_vector(7 downto 0)
		);
	END COMPONENT;
 
	--Inputs
	signal clk : std_logic := '0';
	signal TX_enable : std_logic := '0';
	signal TX_data : std_logic_vector(7 downto 0) := (others => '0');
	--Outputs
	signal TX : std_logic;
	-- Clock period definitions
	constant clk_period : time := 10 ns;
 
	BEGIN
	-- Instantiate the Unit Under Test (UUT)
	uut: UART_TX PORT MAP (
                        clk => clk,
                        TX => TX,
                        TX_enable => TX_enable,
                        TX_data => TX_data
                        );
	-- Clock process definitions
	clk_process :process
	begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;
 
 
	-- Stimulus process
	stim_proc: process
	begin
		-- hold reset state for at least 100 ns.
		TX_enable<='0';
		wait for 100ns;
		TX_enable<='1';
		TX_data<="10110010";
		wait for clk_period*36;
		TX_enable<='0';
		wait;
	end process;
END;
