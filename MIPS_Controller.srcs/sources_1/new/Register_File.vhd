----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/20/2019 11:56:26 AM
-- Design Name: 
-- Module Name: RegisterFile - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

----------------------------------------------------------------------------------

entity Register_File is
    Port ( clk : in STD_LOGIC;
           ReadRegister1 : in STD_LOGIC_VECTOR (4 downto 0);
           ReadRegister2 : in STD_LOGIC_VECTOR (4 downto 0);
           WriteData : in STD_LOGIC_VECTOR (31 downto 0);
           RegWrite : in STD_LOGIC;
           ReadData1 : out STD_LOGIC_VECTOR (31 downto 0);
           ReadData2 : out STD_LOGIC_VECTOR (31 downto 0);
           WriteRegister : in STD_LOGIC_VECTOR (4 downto 0));
end Register_File;

architecture Behavioral of Register_File is    
    type memory is array(0 to 31) of STD_LOGIC_VECTOR(31 downto 0);         --array van alle 32 registers => 5 bits
    signal registers:memory := (others=>(others=>'0'));                     --signaal maken van alle registers en zet alle registers op 0

begin

    RegisterRead: process(ReadRegister1, ReadRegister2)
    begin 
        if(ReadRegister1="00000") then
            ReadData1 <=(others => '0');
        else
            ReadData1<=registers(conv_integer(ReadRegister1));                  --lezen uit register aangegeven door ReadRegister1
        end if;
        
        if(ReadRegister2="00000") then
            ReadData2 <=(others => '0');
        else
            ReadData2<=registers(conv_integer(ReadRegister2));                  --lezen uit register aangegeven door ReadRegister2
        end if;
        
    end process RegisterRead;      
        

    RegisterWrite: process(WriteData, WriteRegister, RegWrite)
    begin 
        if(RegWrite='1') then
             if(WriteRegister="00000") then
             else
                registers(conv_integer(WriteRegister))<= WriteData;             --naar juist register schrijven, aangegeven door WriteRegister
             end if;
        end if;
            
    end process RegisterWrite;
    
end Behavioral;
