----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/23/2019 03:36:24 PM
-- Design Name: 
-- Module Name: Toplayer_Jumps - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Toplayer_Jumps is
    Port ( Immediate : in STD_LOGIC_VECTOR (25 downto 0);
           PC_IN : in STD_LOGIC_VECTOR (31 downto 0);
           PCInput : in STD_LOGIC_VECTOR (31 downto 0);           --Output van vorige MUX van instructionfetch
           MUXInput : in STD_LOGIC;                              --Controlesignaal voor MUX
           PC_OUT : out STD_LOGIC_VECTOR (31 downto 0));
end Toplayer_Jumps;

architecture Behavioral of Toplayer_Jumps is
-----------------------------------------------------------------
COMPONENT JumpAddress is
    Port ( Immediate : in STD_LOGIC_VECTOR (25 downto 0);
           PC_4 : in STD_LOGIC_VECTOR (3 downto 0);
           JumpAddress : out STD_LOGIC_VECTOR (31 downto 0));
end COMPONENT;
-----------------------------------------------------------------
COMPONENT MUX1 is
    Port ( input_1 : in STD_LOGIC_VECTOR (31 downto 0);
           input_2 : in STD_LOGIC_VECTOR (31 downto 0);
           Control : in STD_LOGIC;
           output  :  out STD_LOGIC_VECTOR (31 downto 0));
end COMPONENT;  
-----------------------------------------------------------------
signal Jump_Address: STD_LOGIC_VECTOR(31 downto 0);

begin
	JA:JumpAddress
	port map(
			 Immediate => Immediate,
	      	 PC_4 => PC_IN(31 downto 28),
	         JumpAddress => Jump_Address
	         );
    MUX: MUX1
    port map(
            input_1 => PCInput,
            input_2 => Jump_Address,
            Control => MUXInput,
            output => PC_OUT
            );
            
end Behavioral;
