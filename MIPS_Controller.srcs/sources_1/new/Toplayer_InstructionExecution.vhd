----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/24/2019 12:44:33 PM
-- Design Name: 
-- Module Name: Toplayer_InstructionExecution - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Toplayer_InstructionExecution is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           Instruction : in STD_LOGIC_VECTOR (31 downto 0);
           RX_in : in STD_LOGIC;
           UART_REC : out STD_LOGIC_VECTOR(31 downto 0);   -- data dat de UART receiver heeft ontvangen
           Jump : out STD_LOGIC;
           Branch_Address : inout STD_LOGIC_VECTOR (31 downto 0);
           ALUOp: out STD_LOGIC_VECTOR(1 downto 0);
           ALU_CTRL: out STD_LOGIC_VECTOR(3 downto 0);
           DATA1: out STD_LOGIC_VECTOR(31 downto 0);
           DATA2: out STD_LOGIC_VECTOR(31 downto 0);
           WRITEREG: out STD_LOGIC_VECTOR(4 downto 0);
           ALU_RESULT: out STD_LOGIC_VECTOR(31 downto 0);
           MUXOutput : out STD_LOGIC;
           MULT_Result: out std_logic_vector(63 downto 0);
           UARTWriteSignal: out std_logic;
           WrittenData: out STD_LOGIC_VECTOR(31 downto 0)
           );
end Toplayer_InstructionExecution;

architecture Behavioral of Toplayer_InstructionExecution is
------------------------------------------------------
COMPONENT Control_Unit is
port (
        OP_Code: in std_logic_vector(5 downto 0);
        reset : in STD_LOGIC;
        ALU_OP: out std_logic_vector(1 downto 0);
        register_dist, jump,branch,memory_read, memory_to_register, memory_write, ALU_src, register_write: out std_logic
        );
end COMPONENT;
-------------------------------------------------------
COMPONENT MUX2 is
    Port ( input_1 : in STD_LOGIC_VECTOR (4 downto 0);
           input_2 : in STD_LOGIC_VECTOR (4 downto 0);
           Control : in STD_LOGIC;
           output  :  out STD_LOGIC_VECTOR (4 downto 0));
end COMPONENT;
-------------------------------------------------------
COMPONENT Register_File is
    Port ( clk : in STD_LOGIC;
           ReadRegister1 : in STD_LOGIC_VECTOR (4 downto 0);
           ReadRegister2 : in STD_LOGIC_VECTOR (4 downto 0);
           WriteData : in STD_LOGIC_VECTOR (31 downto 0);
           RegWrite : in STD_LOGIC;
           ReadData1 : out STD_LOGIC_VECTOR (31 downto 0);
           ReadData2 : out STD_LOGIC_VECTOR (31 downto 0);
           WriteRegister : in STD_LOGIC_VECTOR (4 downto 0));
end COMPONENT;
-------------------------------------------------------
COMPONENT Sign_Extend is
    Port ( input : in STD_LOGIC_VECTOR (15 downto 0);
           output : out STD_LOGIC_VECTOR (31 downto 0));
end COMPONENT;
-------------------------------------------------------
COMPONENT MUX1 is
    Port ( input_1 : in STD_LOGIC_VECTOR (31 downto 0);
           input_2 : in STD_LOGIC_VECTOR (31 downto 0);
           Control : in STD_LOGIC;
           output  :  out STD_LOGIC_VECTOR (31 downto 0));
end COMPONENT;
-------------------------------------------------------
COMPONENT ALU_Control_Unit is
port(
    ALU_OP : in std_logic_vector(1 downto 0);
    ALU_Funct : in std_logic_vector(5 downto 0);
    ALU_Control: out std_logic_vector(3 downto 0)
    );
end COMPONENT;
-------------------------------------------------------
COMPONENT ALU is
port( Input1,Input2 : in std_logic_vector(31 downto 0); -- src1, src2
      ALU_Control : in std_logic_vector(3 downto 0); -- function select
      ALU_Result: out std_logic_vector(31 downto 0); -- ALU Output Result
      shamt  : in std_logic_vector(4 downto 0);
      MULT_Result: out std_logic_vector(63 downto 0);
      zero: out std_logic -- Zero Flag
 );
end COMPONENT;
-------------------------------------------------------
COMPONENT Data_Memory is
    Port ( Address : in STD_LOGIC_VECTOR (31 downto 0);
           WriteData : in STD_LOGIC_VECTOR (31 downto 0);
           MemWrite : in STD_LOGIC;
           MemRead : in STD_LOGIC;
           UART_RX : in STD_LOGIC_VECTOR(31 downto 0);   -- data dat de UART receiver heeft ontvangen
           UARTWrite : inout STD_LOGIC;                    -- controlesignaal om UART receiver data in de Memory te steken
           ReadData : out STD_LOGIC_VECTOR (31 downto 0));
end COMPONENT;
-------------------------------------------------------
COMPONENT UART_RX is
    port(
            clk : IN  std_logic;
            --state : OUT STD_LOGIC_VECTOR(1 downto 0);
            RX : INOUT  std_logic;
            RX_in : IN  std_logic;
            RX_data : OUT  std_logic_vector(31 downto 0)
    );
end COMPONENT;

-------------------------------------------------------
--controller signals
signal ALU_OP_signal: std_logic_vector(1 downto 0); --naar ALUControl
signal register_dist_signal: std_logic;
signal jump_signal: std_logic;
signal branch_signal: std_logic;
signal memory_read_signal: std_logic;
signal memtoreg_signal: std_logic;
signal memwrite_signal: std_logic;
signal ALUsrc_signal: std_logic;
signal regWrite_signal: std_logic;

--other
signal writereg_signal: std_logic_vector(4 downto 0);
signal readdata1_signal: std_logic_vector(31 downto 0);
signal readdata2_signal: std_logic_vector(31 downto 0);
signal ALUInput2: std_logic_vector(31 downto 0);
signal ALU_Control_signal: std_logic_vector(3 downto 0);
signal ALUResult_signal: std_logic_vector(31 downto 0);
signal ZeroFlag_signal: std_logic;
signal DataMemoryOut_signal: std_logic_vector(31 downto 0);
signal writedata_signal: std_logic_vector(31 downto 0);     --vasthangen aan output MUX
--UART
signal UART_RX_SIGNAL :STD_LOGIC_VECTOR(31 downto 0);        
signal UARTWrite_signal :STD_LOGIC;

begin
    CON:    Control_Unit
    port map(
            OP_Code => Instruction(31 downto 26),
            ALU_OP => ALU_OP_signal,
            reset => reset,
            register_dist => register_dist_signal,
            jump => jump_signal,
            branch => branch_signal,
            memory_read => memory_read_signal,
            memory_to_register => memtoreg_signal,
            memory_write => memwrite_signal,
            ALU_src => ALUsrc_signal,
            register_write => regWrite_signal
            );
    WRMUX: MUX2
    port map(
            input_1 => Instruction(20 downto 16),
            input_2 => Instruction(15 downto 11),
            Control => register_dist_signal,
            output => writereg_signal
            );
    REGFILE: Register_File
    port map(
            clk => clk,
            ReadRegister1 => Instruction(25 downto 21),
            ReadRegister2 => Instruction(20 downto 16),
            WriteData => writedata_signal,                  --data uit de ALU of andere operatie
            RegWrite => regWrite_signal,
            ReadData1 => readdata1_signal,
            ReadData2 => readdata2_signal,
            WriteRegister => writereg_signal
            );
    SGE: Sign_Extend
    port map(
            input => Instruction(15 downto 0),
            output => Branch_Address
            );
    BAMUX: MUX1
    port map(
            input_1 => readdata2_signal,
            input_2 => Branch_Address,
            Control => ALUsrc_signal,
            output => ALUInput2
            ); 
   ALUCTRL: ALU_Control_Unit
   port map(
            ALU_OP => ALU_OP_signal,
            ALU_Funct => Instruction(5 downto 0),
            ALU_Control => ALU_Control_signal
            );
   AL:ALU
   port map(
            Input1 => readdata1_signal,
            Input2 => ALUInput2,
            ALU_Control => ALU_Control_signal,
            ALU_Result => ALUResult_signal,
            shamt => Instruction(10 downto 6),
            zero => ZeroFlag_signal,
            MULT_Result => MULT_Result
            );   
   DM: Data_Memory
   port map( 
            Address => ALUResult_signal,
            WriteData => readdata2_signal,
            MemWrite => memwrite_signal,
            MemRead => memory_read_signal,  
            UART_RX => UART_RX_SIGNAL,     
            UARTWrite => UARTWrite_signal,
            ReadData => DataMemoryOut_signal
            );
   OUTMUX: MUX1
    port map(
           input_1 => ALUResult_signal,
           input_2 => DataMemoryOut_signal,
           Control => memtoreg_signal,
           output => writedata_signal
           ); 
   RXUART: UART_RX
   port map(
           clk => clk,
           RX => UARTWrite_signal,
           RX_in => RX_in,
           RX_data => UART_RX_SIGNAL
       );
           
   Jump <= jump_signal;
   MUXOutput <= branch_signal and ZeroFlag_signal; --AND GATE
   WrittenData <= writedata_signal;
   ALUOp <= ALU_OP_signal;
   ALU_CTRL <= ALU_Control_signal;
   ALU_RESULT <= ALUResult_signal;
   DATA1 <= readdata1_signal;
   DATA2 <= ALUInput2;
   WRITEREG <= writereg_signal;   
   UARTWriteSignal <= UARTWrite_signal; --signaal dat ingelezen UART waarde in memory steekt
   UART_REC <= UART_RX_SIGNAL;
end Behavioral;
