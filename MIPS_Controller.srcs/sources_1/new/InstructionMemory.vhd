library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity InstructionMemory is
    Port ( clk       : in STD_LOGIC;
           Adress    : in STD_LOGIC_VECTOR(31 downto 0);            -- PC_OUT
           OPcode    : out STD_LOGIC_VECTOR(31 downto 0));
end InstructionMemory;

architecture Behavioral of InstructionMemory is
begin

MemoryPC: process(clk)
    subtype word is STD_LOGIC_VECTOR(31 downto 0);
    type memory is array(0 to 127) of word;         --maakt een klasse van 128 words (32 bit) => 32*4 = 128
    variable myMemory:memory :=                     --maakt instance van de klasse, := omdat het een variabele is
        (--"00100000001000010000000000000101",        -- addi reg1, reg1, 5 => 5
         --"00100000011000110000000000000001",        -- addi reg3, reg3, 1 => 1
         --"00000000001000110001000000100000",        -- add reg1, reg3,  => 6
         --"00100000011000110000000000001101",        -- addi reg3, reg3, d => e
         --"10101100011000100000000000000000",        -- sw reg2, 0(reg3) => e
         --"10001100011011000000000000000000",        -- lw reg12, reg3   => e
         --"00000001100000110010100000100000",        -- add reg12, reg3, reg5 => e + 6 = 14(hex)   
         
         --UART send
         --"00100000001000010000000010000000",        -- addi reg1 => 128 = control register   
         --"00100000010000100000000000000111",        -- addi reg2 => commando voor UART controller => reg 3 eerste byte sturen
         --"10101100001000100000000000000000",        -- sw reg1(rs), reg2(rt)
         
         "10001100011011000000000010000001",        -- lw reg12 => UART waarde
         "10001100011011000000000000000001",        -- lw reg12 => UART waarde
         "00000001100000110010100000100000",        -- add reg12, reg3, reg5 => e + 6 = 14(hex)
         
         "00100000011000110000000000000001",        -- addi reg3, reg3, 1 => 7

         -- 001000 00100 00100 0000000000000010
         "00100000100001000000000000000010",        -- addi reg4, reg4, 2 => 2
         -- 000100 00001 00001 0000000000001000
         --"00010000001000010000000000000011",        -- beq  reg1, reg1, 3 --> +12 in totaal 
         "00010100001000100000000000000011",        -- bnq  reg1, reg2, 3 --> +12 in totaal 
         -- 000000 00001 00100 00101 00000 011000
         "00000000001001000010100000011000",        -- mul  reg5, reg1, reg4 => 6 * 2 = 12      --aluresult geeft 8 omdat het nog optelt van de vorige
         "00000000001001000000100000010010",        -- move Lo to reg1
         "00100000001000010000000000000101",        -- addi reg1(rd), reg1(rs), 6(rt) => 17
         -- 000000 00001 00100 00101 00000 011010
         "00000000001001000010100000011010",        -- div  reg5, reg1, reg4 => 17 / 2 = 8.5
         "00000000001001000000100000010010",        -- move Lo to reg1
         "00000000001001000000100000010000",        -- move Hi to reg1
         others => (others=>'0'));

         
begin    
    --if(rising_edge(clk)) then       
        OPcode <= myMemory(conv_integer(Adress(31 downto 2)));      -- downwto 2, omdat instructie 4 bytes is.
                                                                    -- Dit is equivalent aan delen door 4, wat bekomen kan worden door de twee LSB weg te laten 
    --end if;
end process MemoryPC;

end Behavioral;
