----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/23/2019 04:02:33 PM
-- Design Name: 
-- Module Name: Controller - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity Control_Unit is
port (
        OP_Code: in std_logic_vector(5 downto 0);
        reset: in std_logic;
        ALU_OP: out std_logic_vector(1 downto 0);
        register_dist, jump,branch,memory_read, memory_to_register, memory_write, ALU_src, register_write: out std_logic
        );
end Control_Unit;

architecture Behavioral of Control_Unit is
begin
process(OP_Code)
begin
 if(reset = '1') then
    register_dist <= '0';
    jump <= '0';
    branch <= '0';
    memory_read <= '0';
    memory_to_register <= '0';
    memory_write <= '0';
    alu_src <= '0';
    register_write <= '0'; -- moet misscchien een delay hebben (AFTER 10 ns) en in het begin eerst resetten naar 0
    ALU_OP <= "00";
 else 
     case OP_Code is
         when "000000" =>                 -- R instructions
                register_dist <= '1';
                alu_src <= '0';
                memory_to_register <= '0';
                register_write <= '1';
                memory_read <= '0';
                memory_write <= '0';
                branch <= '0';
                jump <= '0';               
                ALU_OP <= "10";               
         when "000010" =>                 -- jump      hex(0x02)
                register_dist <= '0';
                alu_src <= '0';
                memory_to_register <= '0';
                register_write <= '0';
                memory_read <= '0';
                memory_write <= '0';
                branch <= '0';
                jump <= '1';               
                ALU_OP <= "11";            --ALU wordt niet gebruikt => 11
         when "100011" =>                  -- load word    hex(0x23)
                register_dist <= '0';
                alu_src <= '1';
                memory_to_register <= '1';
                register_write <= '1';
                memory_read <= '1';
                memory_write <= '0';
                branch <= '0';
                jump <= '0';               
                ALU_OP <= "00";
         when "101011" =>                  -- store word    hex(0x2B)
                register_dist <= '0';
                alu_src <= '1';
                memory_to_register <= '0';
                register_write <= '0';
                memory_read <= '0';
                memory_write <= '1';
                branch <= '0';
                jump <= '0';               
                ALU_OP <= "00";
         when "000100" =>                   -- Branch on equal  hex(0x04)
                register_dist <= '0';
                alu_src <= '0';
                memory_to_register <= '0';
                register_write <= '0';
                memory_read <= '0';
                memory_write <= '0';
                branch <= '1';
                jump <= '0';               
                ALU_OP <= "01";
         when "000101" =>                   -- Branch on not equal  hex(0x04)
                register_dist <= '0';
                alu_src <= '0';
                memory_to_register <= '0';
                register_write <= '0';
                memory_read <= '0';
                memory_write <= '0';
                branch <= '1';
                jump <= '0';               
                ALU_OP <= "11";
         when "001000" =>                          -- addi         hex(0x08)
                register_dist <= '0';
                alu_src <= '1';
                memory_to_register <= '0';
                register_write <= '1';
                memory_read <= '0';
                memory_write <= '0';
                branch <= '0';
                jump <= '0';               
                ALU_OP <= "00";
         when others =>
               register_dist <= '0';
               jump <= '0';
               branch <= '0';
               memory_read <= '0';
               memory_to_register <= '0';
               memory_write <= '0';
               alu_src <= '0';
               register_write <= '0';
               ALU_OP <= "00";
              
        end case;
 end if;
end process;
end Behavioral;
