----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/23/2019 03:56:13 PM
-- Design Name: 
-- Module Name: Toplayer_FetchAndJump - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Toplayer_FetchAndJump is
    Port ( PC_IN : in STD_LOGIC_VECTOR (31 downto 0);
           reset : in STD_LOGIC;
           clk : in STD_LOGIC;
           MUXInput1: in STD_LOGIC;                                  --MUX1 (naar MUXOutput)
           MUXInput2 : in STD_LOGIC;                                 --MUX2 (naar Jump)
           ShiftLeftInput: in STD_LOGIC_VECTOR (31 downto 0);        --Input bij branch (naar Branch_Address)
           PC_OUT : out STD_LOGIC_VECTOR (31 downto 0);
           Instruction : inout STD_LOGIC_VECTOR (31 downto 0));
end Toplayer_FetchAndJump;

architecture Behavioral of Toplayer_FetchAndJump is
-----------------------------------------------------
COMPONENT TopLayer_Instruction_Fetch is
    Port ( clk : in STD_LOGIC;
    	   reset: in STD_LOGIC;
           PC_old : in STD_LOGIC_VECTOR (31 downto 0);
           MUXInput: in STD_LOGIC;
           ShiftLeftInput: in STD_LOGIC_VECTOR (31 downto 0);
           PC_4: out STD_LOGIC_VECTOR(31 downto 0);
           PC_new : out STD_LOGIC_VECTOR (31 downto 0);
           Instruction : out STD_LOGIC_VECTOR (31 downto 0));           
end COMPONENT;
-----------------------------------------------------
COMPONENT Toplayer_Jumps is
    Port ( Immediate : in STD_LOGIC_VECTOR (25 downto 0);
           PC_IN : in STD_LOGIC_VECTOR (31 downto 0);
           PCInput : in STD_LOGIC_VECTOR (31 downto 0);           --Output van vorige MUX van instructionfetch
           MUXInput : in STD_LOGIC;                              --Controlesignaal voor MUX
           PC_OUT : out STD_LOGIC_VECTOR (31 downto 0));
end COMPONENT;
-----------------------------------------------------
signal immediate: STD_LOGIC_VECTOR(31 downto 0);
signal PC4: STD_LOGIC_VECTOR(31 downto 0);
signal MUXdata: STD_LOGIC_VECTOR(31 downto 0);

begin
    TIF:TopLayer_Instruction_Fetch
    port map(
            clk => clk,
            reset => reset,
            PC_old => PC_IN,
            MUXInput => MUXInput1,
            ShiftLeftInput => ShiftLeftInput,
            PC_4 => PC4,
            PC_new => MUXdata,
            Instruction => immediate);
    
    TJ:TopLayer_Jumps
    port map(
            Immediate => immediate(25 downto 0),
            PC_IN => PC4, 
            PCInput => MUXdata,
            MUXInput => MUXInput2,
            PC_OUT => PC_OUT);
    
    Instruction <= immediate;   


end Behavioral;
