----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/22/2019 03:29:59 PM
-- Design Name: 
-- Module Name: JumpAddress - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity JumpAddress is
    Port ( Immediate : in STD_LOGIC_VECTOR (25 downto 0);
           PC_4 : in STD_LOGIC_VECTOR (3 downto 0);
           JumpAddress : out STD_LOGIC_VECTOR (31 downto 0));
end JumpAddress;

architecture Behavioral of JumpAddress is

begin
    process(Immediate, PC_4)
    begin
        JumpAddress(1 downto 0) <= (others=>'0');  -- is zelfde als shiftleft2
        JumpAddress(27 downto 2) <= Immediate;
        JumpAddress(31 downto 28) <= PC_4;
    end process;
    
end Behavioral;
