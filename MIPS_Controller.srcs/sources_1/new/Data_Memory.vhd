----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/20/2019 01:50:02 PM
-- Design Name: 
-- Module Name: DataMemory - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Data_Memory is
    Port ( Address : in STD_LOGIC_VECTOR (31 downto 0);
           WriteData : in STD_LOGIC_VECTOR (31 downto 0);
           MemWrite : in STD_LOGIC;
           MemRead : in STD_LOGIC;
           UART_RX : in STD_LOGIC_VECTOR(31 downto 0);   -- data dat de UART receiver heeft ontvangen
           UARTWrite : in STD_LOGIC;                    -- controlesignaal om UART receiver data in de Memory te steken
           ReadData : out STD_LOGIC_VECTOR (31 downto 0));
end Data_Memory;

architecture Behavioral of Data_Memory is
            type memory is array(0 to 255) of STD_LOGIC_VECTOR(31 downto 0);            --array heeft 128 woorden plaats
            signal mem:memory  := (others=>(others=>'0'));                              --memory allemaal op 0 zetten                                         
begin

    DataProcess:process(Address, WriteData, MemWrite, MemRead)
        
    begin
        if(MemRead = '1') then
            ReadData<= mem(conv_integer(Address));          --Data uit geheugen uitlezen
        elsif(MemWrite='1') then        
            mem(conv_integer(Address)) <= WriteData;        --Data in geheugen schrijven
        end if;
    
    end process DataProcess;
    
    UARTReceiveProcess: process(UART_RX, UARTWrite, WriteData)
    
    --variable StoreAddress : integer := 129;    
    begin
        if(UARTWrite = '1') then
            mem(1) <= UART_RX;
            --StoreAddress := StoreAddress + 1;
        end if;      
    
    end process UARTReceiveProcess;

end Behavioral;
