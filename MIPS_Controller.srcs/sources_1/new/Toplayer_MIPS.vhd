----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/25/2019 09:25:42 PM
-- Design Name: 
-- Module Name: Toplayer_MIPS - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Toplayer_MIPS is
    Port ( clk : in STD_LOGIC;
           PC_IN : in STD_LOGIC_VECTOR (31 downto 0);
           Reset : in STD_LOGIC;
           RX_in : in STD_LOGIC;
           Instruction : out STD_LOGIC_VECTOR (31 downto 0);                        -- Instructie die uitgevoerd wordt
           UART_REC : out STD_LOGIC_VECTOR(31 downto 0);   -- data dat de UART receiver heeft ontvangen
           ALUOp: out STD_LOGIC_VECTOR(1 downto 0);
           ALU_CTRL: out STD_LOGIC_VECTOR(3 downto 0);
           DATA1: out STD_LOGIC_VECTOR(31 downto 0);
           DATA2: out STD_LOGIC_VECTOR(31 downto 0);
           WRITEREG: out STD_LOGIC_VECTOR(4 downto 0);
           ALU_RESULT: out STD_LOGIC_VECTOR(31 downto 0);
           MULT_Result: out std_logic_vector(63 downto 0);
           MUXOutput : out STD_LOGIC;
           UARTWriteSignal: out std_logic;
           PC_OUT : out STD_LOGIC_VECTOR (31 downto 0);                             -- nieuwe PC die in PC_IN gestoken moet worden
           WrittenData: out STD_LOGIC_VECTOR(31 downto 0)                           -- data die terug gaat naar Register file 
           );
end Toplayer_MIPS;

architecture Behavioral of Toplayer_MIPS is
----------------------------------------------
COMPONENT Toplayer_InstructionExecution is
Port ( clk : in STD_LOGIC;
       reset : in STD_LOGIC;           
       Instruction : in STD_LOGIC_VECTOR (31 downto 0);
       RX_in : in STD_LOGIC;
       UART_REC : out STD_LOGIC_VECTOR(31 downto 0);   -- data dat de UART receiver heeft ontvangen
       Jump : out STD_LOGIC;
       Branch_Address : inout STD_LOGIC_VECTOR (31 downto 0);
       ALUOp: out STD_LOGIC_VECTOR(1 downto 0);
       ALU_CTRL: out STD_LOGIC_VECTOR(3 downto 0);
       DATA1: out STD_LOGIC_VECTOR(31 downto 0);
       DATA2: out STD_LOGIC_VECTOR(31 downto 0);
       WRITEREG: out STD_LOGIC_VECTOR(4 downto 0);
       ALU_RESULT: out STD_LOGIC_VECTOR(31 downto 0);
       MUXOutput : out STD_LOGIC;
       MULT_Result: out std_logic_vector(63 downto 0);
       UARTWriteSignal: out std_logic;
       WrittenData: out STD_LOGIC_VECTOR(31 downto 0)
       );
end COMPONENT;
----------------------------------------------
COMPONENT Toplayer_FetchAndJump is
Port ( PC_IN : in STD_LOGIC_VECTOR (31 downto 0);
       reset : in STD_LOGIC;
       clk : in STD_LOGIC;
       MUXInput1: in STD_LOGIC;                                  --MUX1 (naar MUXOutput)
       MUXInput2 : in STD_LOGIC;                                 --MUX2 (naar Jump)
       ShiftLeftInput: in STD_LOGIC_VECTOR (31 downto 0);        --Input bij branch (naar Branch_Address)
       PC_OUT : out STD_LOGIC_VECTOR (31 downto 0);
       Instruction : inout STD_LOGIC_VECTOR (31 downto 0));
end COMPONENT;
----------------------------------------------
signal Instruction_signal: std_logic_vector(31 downto 0);
signal Jump_signal: std_logic;
signal BranchAddress_signal: std_logic_vector(31 downto 0);
signal Branch_signal: std_logic;


begin
    TIE: Toplayer_InstructionExecution
    port map(clk => clk,
             reset => Reset,
             Instruction => Instruction_signal,
             RX_in => RX_in,
             UART_REC => UART_REC,
             Jump => Jump_signal,
             Branch_Address => BranchAddress_signal,
             ALUOp => ALUOp,
             ALU_CTRL => ALU_CTRL,
             DATA1 => DATA1,
             DATA2 => DATA2,
             WRITEREG => WRITEREG,
             ALU_RESULT => ALU_RESULT,
             MUXOutput => Branch_signal,
             WrittenData => WrittenData,
             UARTWriteSignal=> UARTWriteSignal,
             MULT_Result => MULT_Result
             );
    TFJ: Toplayer_FetchAndJump
    Port map ( PC_IN => PC_IN,
               reset => reset,
               clk => clk,
               MUXInput1 => Branch_signal,                               --MUX1 (naar MUXOutput)
               MUXInput2 => Jump_signal,                                 --MUX2 (naar Jump)
               ShiftLeftInput => BranchAddress_signal,      --Input bij branch (naar Branch_Address)
               PC_OUT => PC_OUT,
               Instruction => Instruction_signal
               );
   
   Instruction <= Instruction_signal;
   MUXOutput <= Branch_Signal;
                

end Behavioral;
