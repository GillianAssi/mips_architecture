----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/11/2020 12:19:08 PM
-- Design Name: 
-- Module Name: UART - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
    ----------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
----------------------------------------------------
entity UART_RX is
    port(
            clk : IN  std_logic;
            --state : OUT STD_LOGIC_VECTOR(1 downto 0);
            RX : OUT  std_logic;
            RX_in : IN  std_logic;
            RX_data : OUT  std_logic_vector(31 downto 0)
    );
end UART_RX;
----------------------------------------------------
architecture Behavioral of UART_RX is
            -- declare a type for our statemachine
    type UART_RX_STATE is (IDLE,STARTBIT,RECEIVE,STOPBIT);
    signal state:UART_RX_STATE:=IDLE;
    signal data:STD_LOGIC_VECTOR(7 downto 0);
    signal databit_counter:unsigned(3 downto 0); -- count the amount of bits sent + 1
    signal  clk_divider:unsigned(13 downto 0):=(others=>'0'); -- count up to 5208 ticks for 9600 baud per second
    begin
    ----------------------------------------------------
    UART_RX:process(clk, RX_in)
    variable counter_cap : integer := 4;
    variable RX_old:STD_LOGIC:='0';
    begin
        if (rising_edge(clk)) then
            case state is
                -------------------
                when IDLE =>
                    if (RX_in='0') then
                        clk_divider<=(others=>'0');
                        databit_counter<=(others=>'0');
                        RX_data<=(others => '0');
                        RX <= '0';                                      -- gewoon ter initialisatie op 0 zetten
                        state<=STARTBIT;
                    end if;
                -------------------
                when STARTBIT=>
                    if (clk_divider=(counter_cap / 2 - 1)) then
                        clk_divider<=(others=>'0');
                        state<=RECEIVE;
                    else    
                        clk_divider<=clk_divider+1;
                    end if;
                -------------------
                when RECEIVE =>
                    if (clk_divider=(counter_cap - 1)) then
                        clk_divider<=(others=>'0');
                        RX_data(to_integer(databit_counter)) <= RX_in;
                        databit_counter<=databit_counter+1;
                        if (databit_counter=7) then
                            RX <= '1';
                            state<=STOPBIT;
                        end if;
                    else    
                        clk_divider<=clk_divider+1;
                    end if;
                -------------------
                when STOPBIT =>
                    RX <= '0';
                    if (clk_divider=(counter_cap / 2 - 1)) then
                        clk_divider<=(others=>'0');
                        if (RX_in='1') then
                            state<=IDLE;
                        end if;
                    else    
                        clk_divider<=clk_divider+1;
                    end if;
                -------------------    
                when others  => NULL;
            end case;
        end if;
    end process UART_RX;
----------------------------------------------------
end Behavioral;
----------------------------------------------------