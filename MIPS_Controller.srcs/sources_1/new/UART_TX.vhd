----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/11/2020 12:19:08 PM
-- Design Name: 
-- Module Name: UART - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
    ----------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
----------------------------------------------------
entity UART_TX is
    port(
            clk : IN  std_logic;
            --state : OUT STD_LOGIC_VECTOR(1 downto 0);
            TX : OUT  std_logic;                        --output van de bits
            TX_enable : IN  std_logic;                  --enable signaal
            TX_data : IN  std_logic_vector(7 downto 0)
    );
end UART_TX;
----------------------------------------------------
architecture Behavioral of UART_TX is
            -- declare a type for our statemachine
    type UART_TX_STATE is (IDLE,STARTBIT,SENDDATA, STOPBIT);
    signal state:UART_TX_STATE:=IDLE;
    signal data:STD_LOGIC_VECTOR(7 downto 0);
    signal databit_counter:unsigned(3 downto 0); -- count the amount of bits sent + 1
    signal  clk_divider:unsigned(13 downto 0):=(others=>'0'); -- count up to 5208 ticks for 9600 baud per second
    begin
    ----------------------------------------------------
    UART_TX:process(clk, TX_enable)
    variable counter_cap : integer := 4;
    variable TX_old:STD_LOGIC:='0';
    begin
        if (rising_edge(clk)) then
            case state is
                -------------------
                when IDLE =>
                    if (TX_enable='1') then
                        clk_divider<=(others=>'0');
                        databit_counter<=(others=>'0');
                        state<=STARTBIT;
                        data<=TX_data;
                    else 
                        TX<='1';
                    end if;
                -------------------
                when STARTBIT=>
                    if (clk_divider=(counter_cap - 1)) then
                        clk_divider<=(others=>'0');
                        TX<='0';
                        state<=SENDDATA;
                    else    
                        clk_divider<=clk_divider+1;
                    end if;
                -------------------
                when SENDDATA =>
                    if (clk_divider=(counter_cap - 1)) then
                        clk_divider<=(others=>'0');
                        TX<=data(to_integer(databit_counter));
                        databit_counter<=databit_counter+1;
                        if (databit_counter=7) then
                            state<=STOPBIT;
                        end if;
                    else    
                        clk_divider<=clk_divider+1;
                    end if;
                -------------------
                when STOPBIT =>
                    if (clk_divider=(counter_cap - 1)) then
                        clk_divider<=(others=>'0');
                        TX<='1';
                        if (TX_enable='0') then
                            state<=IDLE;
                        end if;
                    else    
                        clk_divider<=clk_divider+1;
                    end if;
                -------------------    
                when others  => NULL;
            end case;
        end if;
    end process UART_TX;
----------------------------------------------------
end Behavioral;
----------------------------------------------------