----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/23/2019 06:03:22 PM
-- Design Name: 
-- Module Name: ALU_Control_Unit - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ALU_Control_Unit is
port(
    ALU_OP : in std_logic_vector(1 downto 0);
    ALU_Funct : in std_logic_vector(5 downto 0);
    ALU_Control: out std_logic_vector(3 downto 0)
    );
end ALU_Control_Unit;

architecture Behavioral of ALU_Control_Unit is
begin
    process(ALU_OP,ALU_Funct)
    begin
        case ALU_OP is
        when "00" =>                                --always add
            ALU_Control <= "0010";
        when "01" =>                                --always subtract
            ALU_Control <= "0100";                                  
        when "10" =>                                --dependant on Funct
            case ALU_funct is 
                when "100000" =>                    --add 
                    ALU_Control <= "0010";
                when "100001" =>                    --addu 
                    ALU_Control <= "0011";
                when "100010" =>                    --sub 
                    ALU_Control <= "0110";
                when "100100" =>                    --and 
                    ALU_Control <= "0000";
                when "100101" =>                    --or
                    ALU_Control <= "0001";
                when "101010" =>                    --slt (set if less than)
                    ALU_Control <= "0111";
                when "010000" =>                    --get Hi
                    ALU_Control <= "0101";
                when "010010" =>                    --get Lo 
                    ALU_Control <= "0110";
                when "000000" =>                    --sll 
                    ALU_Control <= "1001";
                when "000010" =>                    --srl 
                    ALU_Control <= "1010";
                when "000011" =>                    --srla
                    ALU_Control <= "1011";
                when "011000" =>                    --mul
                    ALU_Control <= "1101";
                when "011010" =>                    --mul
                    ALU_Control <= "1110";
                when others => null;
            end case;
        when "11" => 
            ALU_Control <= "1111";    
        when others => null;            
        end case;
    end process;
end Behavioral;