----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/23/2019 04:06:19 PM
-- Design Name: 
-- Module Name: ALU - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_signed.all;
use IEEE.NUMERIC_STD.ALL;

-- VHDL code for ALU of the MIPS Processor
entity ALU is
port(
 Input1,Input2 : in std_logic_vector(31 downto 0); -- src1, src2
 ALU_Control : in std_logic_vector(3 downto 0); -- function select
 shamt  : in std_logic_vector(4 downto 0); --shift amount
 ALU_Result: out std_logic_vector(31 downto 0); -- ALU Output Result
 MULT_Result : out std_logic_vector(63 downto 0);
 zero: out std_logic -- Zero Flag
 );
end ALU;

architecture Behavioral of ALU is
    signal TempResult: std_logic_vector(31 downto 0);
    signal Multp: std_logic_vector(63 downto 0) := (others => '0');
    signal Invert: std_logic := '0';
    
    begin
        process(ALU_Control,Input1,Input2)
        
        begin
             
             case ALU_Control is
             when "0000" => 
              TempResult <= Input1 and Input2;    -- and
             when "0001" =>
              TempResult <= Input1 or Input2;       -- or
             when "0010" =>
              TempResult <= std_logic_vector (signed(Input1) + signed(Input2));             -- add
             when "0011" =>
              TempResult <= std_logic_vector (unsigned(Input1) + unsigned(Input2));         -- addu
             when "0100" =>
              TempResult <= std_logic_vector (signed(Input1) - signed(Input2));             -- sub
             when "0101" =>
              TempResult <= Multp(63 downto 32);                               -- Mov from Hi
             when "0110" =>                                                                     
              TempResult <= Multp(31 downto 0);                                 -- Mov from Lo
             when "0111" =>                          
              if (signed(Input1)<signed(Input2)) then
                TempResult <= "00000000000000000000000000000001";         --slt
              else 
                TempResult <= "00000000000000000000000000000000";
              end if;
             when "1001" =>
              TempResult(31 downto to_integer(unsigned(shamt))) <= Input2(31-to_integer(unsigned(shamt)) downto 0);    --sll
              TempResult(to_integer(unsigned(shamt))-1 downto 0) <= (others=>'0');
             when "1010" =>
              TempResult(31-to_integer(unsigned(shamt)) downto 0) <= Input2(31 downto to_integer(unsigned(shamt)));    --slr 
              if(to_integer(unsigned(shamt)) /= 0) then
               TempResult(31 downto 32-to_integer(unsigned(shamt))) <= (others=>'0');
              end if;
             when "1011" => 
              TempResult(31-to_integer(unsigned(shamt)) downto 0) <= Input2(31 downto to_integer(unsigned(shamt)));    --slra 
              if(to_integer(unsigned(shamt)) /= 0) then
               TempResult(31 downto 32-to_integer(unsigned(shamt))) <= (others=>Input2(31));
              end if;
             when "1101" =>
              Multp <= std_logic_vector(to_unsigned(to_integer(unsigned(input1)) * to_integer(unsigned(input2)), 64));                                        -- multp
             when "1110" =>                                                                     
              Multp(31 downto 0) <= std_logic_vector(to_unsigned(to_integer(unsigned(input1))/ to_integer(unsigned(input2)), 32));              -- aantal deelbaar in lo
              Multp(63 downto 32) <= std_logic_vector(to_unsigned(to_integer(unsigned(input1)) mod to_integer(unsigned(input2)), 32));          -- modulo in hi
             when "1111" =>
              TempResult <= std_logic_vector (signed(Input1) - signed(Input2));             -- sub inv
              Invert <= '1';
             when others => NULL; -- no default
             end case;
        end process;
  zero <= '1' when (TempResult="00000000000000000000000000000000" xnor Invert = '0') else '0';
  Invert <= '0';
  ALU_Result <= TempResult;
  MULT_Result <= Multp;
end Behavioral;