----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/20/2019 12:56:42 PM
-- Design Name: 
-- Module Name: MUX1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-----------------------------------------------------------------------------------

--Algemene MUX

entity MUX1 is
    Port ( input_1 : in STD_LOGIC_VECTOR (31 downto 0);
           input_2 : in STD_LOGIC_VECTOR (31 downto 0);
           Control : in STD_LOGIC;
           output  :  out STD_LOGIC_VECTOR (31 downto 0));
end MUX1;

architecture Behavioral of MUX1 is
begin
    
    MUX:process(Control, input_1, input_2)
    begin
        
        if(Control='0') then
            output<=input_1;
        else
            output<=input_2;
        end if;
    end process;
end Behavioral;
