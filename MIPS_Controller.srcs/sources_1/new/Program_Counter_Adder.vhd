----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/29/2019 08:29:14 AM
-- Design Name: 
-- Module Name: Program_Counter_Adder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Program_Counter_Adder is
port(  clk       : in STD_LOGIC;
       INPUT_0   : in STD_LOGIC_VECTOR(31 downto 0);                    -- PC_OUT
       --INPUT_1   : in STD_LOGIC_VECTOR(31 downto 0);
       OUTPUT    : out STD_LOGIC_VECTOR(31 downto 0));
end Program_Counter_Adder;

architecture Behavioral of Program_Counter_Adder is
begin

Add:Process(clk)
    
begin 
        OUTPUT <= INPUT_0 + 4;
end Process Add;
end Behavioral;
