----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/03/2019 12:25:08 PM
-- Design Name: 
-- Module Name: TopLayer_Instruction_Fetch - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TopLayer_Instruction_Fetch is
    Port ( clk : in STD_LOGIC;
    	   reset: in STD_LOGIC;
           PC_old : in STD_LOGIC_VECTOR (31 downto 0);
           MUXInput: in STD_LOGIC;
           ShiftLeftInput: in STD_LOGIC_VECTOR (31 downto 0);
           PC_4: out STD_LOGIC_VECTOR(31 downto 0);
           PC_new : out STD_LOGIC_VECTOR (31 downto 0);
           Instruction : out STD_LOGIC_VECTOR (31 downto 0));
           
end TopLayer_Instruction_Fetch;

architecture Behavioral of TopLayer_Instruction_Fetch is
------------------------------------
	COMPONENT Program_Counter_Adder is
    Port ( clk : in STD_LOGIC;
           input_0 : in STD_LOGIC_VECTOR (31 downto 0);
           output : out STD_LOGIC_VECTOR (31 downto 0));
	end component;
------------------------------------
	COMPONENT  InstructionMemory is
	    Port (  Clk       : in STD_LOGIC;
                Adress    : in STD_LOGIC_VECTOR(31 downto 0);            -- PC_OUT
                OPcode    : out STD_LOGIC_VECTOR(31 downto 0));
	end component ;
------------------------------------
	COMPONENT Program_counter is
    Port (  Clk     : in STD_LOGIC;
            reset   : in STD_LOGIC;
            PC_IN   : in STD_LOGIC_VECTOR(31 downto 0);
            PC_OUT   : out STD_LOGIC_VECToR(31 downto 0));
	end COMPONENT;
------------------------------------
    COMPONENT MUX1 is
    Port ( input_1 : in STD_LOGIC_VECTOR (31 downto 0);
           input_2 : in STD_LOGIC_VECTOR (31 downto 0);
           Control : in STD_LOGIC;
           output  :  out STD_LOGIC_VECTOR (31 downto 0));
    end COMPONENT;
------------------------------------
    COMPONENT ShiftLeft is
    Port ( input : in STD_LOGIC_VECTOR (31 downto 0);
           output : out STD_LOGIC_VECTOR (31 downto 0));
    end COMPONENT;
------------------------------------
    COMPONENT Branch_Adder is
    Port ( input_0 : in STD_LOGIC_VECTOR (31 downto 0);
           input_1 : in STD_LOGIC_VECTOR (31 downto 0);
           output : out STD_LOGIC_VECTOR (31 downto 0));
    end COMPONENT;
    --__________________________________________________________________________________________________
signal Program_Adress: std_logic_vector(31 downto 0);
signal PC_Added: std_logic_vector(31 downto 0);
signal Branch_Address: std_logic_vector(31 downto 0);
signal Shifted_Instruction: std_logic_vector(31 downto 0);

begin
	PCA:Program_Counter_Adder
	port map(
			 clk=>clk,
	      	 input_0=>Program_Adress,
	         output=>PC_Added
	         );
	IM:InstructionMemory
	port map(
			  clk=>clk,
		      Adress=>Program_Adress,
		      OPcode=>Instruction
			 );
	PC:Program_Counter
	port map(
			 clk=>clk,
			 reset=>reset,
			 PC_IN=>PC_old,
			 PC_OUT=>Program_Adress
			 );	
    MUX:MUX1
    port map(
            input_1 => PC_Added,
            input_2 => Branch_Address,
            Control => MUXInput,
            output  => PC_new
            );
    SL:ShiftLeft
        port map(
            input => ShiftLeftInput,
            output => Shifted_Instruction
                );
                
    BA:Branch_Adder
        port map(
            input_0 => PC_Added,
            input_1 => Shifted_Instruction,
            output => Branch_Address
                );
    PC_4 <= PC_Added;
end Behavioral;
