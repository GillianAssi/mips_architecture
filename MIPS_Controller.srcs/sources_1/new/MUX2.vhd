----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/20/2019 12:56:42 PM
-- Design Name: 
-- Module Name: MUX1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-----------------------------------------------------------------------------------

--Algemene MUX

entity MUX2 is
    Port ( input_1 : in STD_LOGIC_VECTOR (4 downto 0);
           input_2 : in STD_LOGIC_VECTOR (4 downto 0);
           Control : in STD_LOGIC;
           output  :  out STD_LOGIC_VECTOR (4 downto 0));
end MUX2;

architecture Behavioral of MUX2 is
begin
    
    MUX:process(Control, input_1, input_2)
    begin
        
        if(Control='0') then
            output<=input_1;
        else
            output<=input_2;
        end if;
    end process;
end Behavioral;
