#!/bin/bash -f
xv_path="/home/edsp_lab/Xilinx/Vivado/2016.4"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xelab -wto 2adafc2d5c8e4e01bc1e959e0173f5dd -m64 --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot Simulated_MIPS_behav xil_defaultlib.Simulated_MIPS -log elaborate.log
