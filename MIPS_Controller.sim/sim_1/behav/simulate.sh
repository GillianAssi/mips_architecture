#!/bin/bash -f
xv_path="/home/edsp_lab/Xilinx/Vivado/2016.4"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xsim Simulated_MIPS_behav -key {Behavioral:sim_1:Functional:Simulated_MIPS} -tclbatch Simulated_MIPS.tcl -log simulate.log
